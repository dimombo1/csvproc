#ifndef EXPRS_HPP
#define EXPRS_HPP

namespace wrk
{
  int sum(int, int);
  int substract(int, int);
  int divide(int, int);
  int multiply(int, int);
  int moduloOperation(int, int);
}

#endif
