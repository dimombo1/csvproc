#ifndef CSV_IO_HPP
#define CSV_IO_HPP

#include <iosfwd>
#include <vector>
#include <string>
#include <map>

namespace wrk
{
  using StrTable = std::vector< std::vector< std::string > >;

  struct StrTableIO
  {
    StrTable & strTable_;
  };

  using Cells = std::map< std::string, std::map< int, int > >;

  struct CellsIO
  {
    Cells & cells_;
  };

  std::istream & operator>>(std::istream & in, StrTableIO &&);
  std::ostream & operator<<(std::ostream & out, const StrTableIO &&);
  Cells getTransformedTable(const StrTable &);
  StrTable cellsToStrTable(const Cells &);
  std::istream & operator>>(std::istream & in, CellsIO &&);
  std::ostream & operator<<(std::ostream & out, const CellsIO &&);
}

#endif
