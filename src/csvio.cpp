#include "csvio.hpp"
#include <iostream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <map>
#include <tuple>
#include <string>
#include <cctype>

#include "exprs.hpp"

namespace
{
  std::vector< std::string > split(const std::string & line, char delimiter)
  {
    std::vector< std::string > tokens;
    std::stringstream ss(line);
    std::string token;

    while (std::getline(ss, token, delimiter))
    {
      tokens.push_back(token);
    }

    return tokens;
  }

  std::ostream & printLine(std::ostream & out, const std::vector< std::string > & line)
  {
    if (!line.size())
    {
      return out;
    }
    for (size_t j = 0; j < line.size() - 1; ++j)
    {
      out << line[j] << ",";
    }
    out << line[line.size() - 1];
    return out;
  }

  using Values = std::vector< std::tuple< std::string, int, std::string > >;

  std::pair< std::string, int > splitArgs(const std::string & arg)
  {
    auto arg_pos = std::find_if(arg.begin(), arg.end(), isdigit);
    if (arg_pos == arg.begin())
    {
      return std::pair< std::string, int >{ "", std::stoi(arg) };
    }
    std::string col = arg.substr(0, std::distance(arg.begin(), arg_pos));
    int pos = std::stoi(arg.substr(std::distance(arg.begin(), arg_pos)));
    std::pair< std::string, int > coord{ col, pos };
    return coord;
  }

  bool isop(char c)
  {
    return (c == '+') || (c == '*') || (c == '/') || (c == '-') || (c == '%');
  }

  void calculateExprs(wrk::Cells & data, const Values & values)
  {
    for (auto && value: values)
    {
      const std::string & col_name = std::get< 0 >(value);
      int pos = std::get< 1 >(value);
      const std::string & cell = std::get< 2 >(value);
      if (cell[0] != '=')
      {
        throw std::logic_error("UNKNOWN VALUE OF THE CELL");
      }
      std::string expr = cell.substr(1);

      auto op_ptr = std::find_if(std::begin(expr), std::end(expr), isop);
      std::string arg1 = expr.substr(0, std::distance(std::begin(expr), op_ptr));
      auto first_coord = splitArgs(arg1);
      int value1 = first_coord.first.empty() ? first_coord.second
                                             : data.at(first_coord.first).at(first_coord.second);
      std::string arg2 = expr.substr(std::distance(std::begin(expr), op_ptr) + 1);
      auto second_coord = splitArgs(arg2);
      int value2 = second_coord.first.empty() ? second_coord.second
                                              : data.at(second_coord.first).at(second_coord.second);
      int result = 0;
      char op = *op_ptr;
      if (op == '+')
      {
        result = wrk::sum(value1, value2);
      }
      else if (op == '-')
      {
        result = wrk::substract(value1, value2);
      }
      else if (op == '*')
      {
        result = wrk::multiply(value1, value2);
      }
      else if (op == '/')
      {
        result = wrk::divide(value1, value2);
      }
      else if (op == '%')
      {
        result = wrk::moduloOperation(value1, value2);
      }
      else
      {
        throw std::logic_error("NO_SUCH_OP");
      }
      data.at(col_name).insert(std::pair< int, int >(pos, result));
    }
  }
}

std::istream & wrk::operator>>(std::istream & in, wrk::StrTableIO && str_table)
{
  std::istream::sentry sentry(in);
  const std::string WRONG_FORMATTED = "WRONG_FORMATTED_TABLE";
  if (!sentry)
  {
    return in;
  }

  std::string line;

  size_t number_of_cols = 0;
  std::getline(in, line);
  if (!line.empty())
  {
    auto row = split(line, ',');
    str_table.strTable_.push_back(row);
    number_of_cols = row.size();
  }

  while (std::getline(in, line))
  {
    if (!line.empty())
    {
      auto row = split(line, ',');
      if (row.size() != number_of_cols)
      {
        throw std::logic_error(WRONG_FORMATTED);
      }
      str_table.strTable_.push_back(row);
    }
  }

  return in;
}

std::ostream & wrk::operator<<(std::ostream & out, const wrk::StrTableIO && str_table)
{
  std::ostream::sentry sentry(out);
  if (!sentry)
  {
    return out;
  }

  out << ',';
  wrk::StrTable & table = str_table.strTable_;
  for (size_t i = 0; i < table.size() - 1; ++i)
  {
    std::vector< std::string > & line = table[i];
    printLine(out, line);
    out << '\n';
  }
  printLine(out, table[table.size() - 1]);
  return out;
}

wrk::Cells wrk::getTransformedTable(const wrk::StrTable & src_table)
{
  wrk::Cells data;
  Values remains;
  for (size_t i = 0; i < src_table[1].size(); ++i)
  {
    std::map< int, int > col;
    const std::string & col_name = src_table[0][i];
    for (size_t j = 1; j < src_table.size(); ++j)
    {
      const std::string & cell = src_table[j][i];
      if (cell[0] != '=')
      {
        int value = std::stoi(src_table[j][i]);
        int pos = std::stoi(src_table[j][0]);
        col.insert({ pos, value });
      }
      else
      {
        remains.push_back({ col_name, j, src_table[j][i] });
      }
    }
    data.insert({ col_name, col });
  }
  calculateExprs(data, remains);
  return data;
}

wrk::StrTable wrk::cellsToStrTable(const wrk::Cells & cells)
{
  wrk::StrTable table;
  for (auto && col: cells)
  {
    std::vector< std::string > col_str;
    col_str.push_back(col.first);
    for (auto && elem: col.second)
    {
      col_str.push_back(std::to_string(elem.second));
    }
    table.push_back(col_str);
  }

  for (size_t i = 0; i < table.size(); ++i)
  {
    for (size_t j = 0; j < i; ++j)
    {
      std::swap(table[i][j], table[j][i]);
    }
  }
  table[0].erase(table[0].begin());
  return table;
}

std::ostream & wrk::operator<<(std::ostream & out, const wrk::CellsIO && cellsio)
{

  std::ostream::sentry sentry(out);
  if (!sentry)
  {
    return out;
  }
  wrk::StrTable table = wrk::cellsToStrTable(cellsio.cells_);
  out << wrk::StrTableIO{ table };
  return out;
}
