#include <iostream>
#include <fstream>
#include "csvio.hpp"

int main(int argc, char ** argv)
{
  const std::string ERR = "WRONG NUMBER OF ARGS\n";
  if (argc != 2)
  {
    std::cerr << ERR;
    return 1;
  }
  try
  {
    std::string filename = argv[1];
    std::fstream file(filename, std::ios_base::in);
    wrk::StrTable table;
    file >> wrk::StrTableIO{ table };
    auto cells = wrk::getTransformedTable(table);
    std::cout << wrk::CellsIO{ cells };
  }
  catch (const std::out_of_range & e)
  {
    std::cerr << "WRONG ADDRESS OF CELL IN OPERATION";
  }
  catch (const std::exception & e)
  {
    std::cerr << e.what() << '\n';
    return 2;
  }
  std::cout << '\n';
  return 0;
}
