#include "exprs.hpp"
#include <cmath>
#include <stdexcept>
#include <limits>

namespace wrk
{
  int MAX = std::numeric_limits< int >::max();
  int MIN = std::numeric_limits< int >::min();
  bool isOvflSum(int, int);
  bool isUnflSum(int, int);
  bool isOvflMult(int, int);
  bool isUnflMult(int, int);
  bool isSumOverflow(int lhs, int rhs)
  {
    return isOvflSum(lhs, rhs) || isUnflSum(lhs, rhs);
  }
  bool isMultOverflow(int lhs, int rhs)
  {
    return isOvflMult(lhs, rhs) || isUnflMult(lhs, rhs);
  }
  bool isDivOverflow(int lhs, int rhs)
  {
    return lhs == MIN && rhs == -1;
  }
  int sum(int lhs, int rhs)
  {
    if (isSumOverflow(lhs, rhs))
    {
      throw std::overflow_error("Arithmetic overflow occured");
    }
    return lhs + rhs;
  }
  int substract(int lhs, int rhs)
  {
    if (isSumOverflow(lhs, -rhs))
    {
      throw std::overflow_error("Arithmetic overflow occured");
    }
    return lhs - rhs;
  }
  int divide(int lhs, int rhs)
  {
    if (isDivOverflow(lhs, rhs))
    {
      throw std::overflow_error("Arithmetic overflow occured");
    }
    if (!rhs)
    {
      throw std::logic_error("Division by zero");
    }
    return lhs / rhs;
  }
  int multiply(int lhs, int rhs)
  {
    if (isMultOverflow(lhs, rhs))
    {
      throw std::overflow_error("Arithmetic overflow occured");
    }
    return lhs * rhs;
  }
  int moduloOperation(int lhs, int rhs)
  {
    if (!rhs)
    {
      throw std::logic_error("Division by zero");
    }
    else if ((lhs > 0 && rhs > 0) || (lhs > 0 && rhs < 0))
    {
      return lhs % std::abs(rhs);
    }
    else
    {
      return lhs - rhs * (divide(lhs, rhs) - 1);
    }
  }
  bool isOvflSum(int lhs, int rhs)
  {
    return lhs > 0 && rhs > 0 && (MAX - lhs < rhs);
  }
  bool isUnflSum(int lhs, int rhs)
  {
    return lhs < 0 && rhs < 0 && (MIN + std::abs(lhs) > rhs);
  }
  bool isSameSign(int lhs, int rhs)
  {
    return (lhs > 0 && rhs > 0) || (lhs < 0 && rhs < 0);
  }
  bool isDiffSign(int lhs, int rhs)
  {
    return (lhs > 0 && rhs < 0) || (lhs < 0 && rhs > 0);
  }
  bool isOvflMult(int lhs, int rhs)
  {
    return isSameSign(lhs, rhs) && (MAX / std::abs(lhs) < std::abs(rhs));
  }
  bool isUnflMult(int lhs, int rhs)
  {
    return isDiffSign(lhs, rhs) && (MIN / std::abs(lhs) > -std::abs(rhs));
  }
}
